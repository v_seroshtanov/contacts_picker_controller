//
//  UIView+RKTContacts.h
//  ContactFrameworkTest
//
//  Created by Виталий Сероштанов on 10.07.17.
//  Copyright © 2017 Виталий Сероштанов. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (RKTContacts)

- (void)roundWithBborderRadius:(CGFloat)radius borderWidth:(CGFloat)border color:(UIColor*)color;

@end
