//
//  UIView+RKTContacts.m
//  ContactFrameworkTest
//
//  Created by Виталий Сероштанов on 10.07.17.
//  Copyright © 2017 Виталий Сероштанов. All rights reserved.
//

#import "UIView+RKTContacts.h"

@implementation UIView (RKTContacts)

- (void)roundWithBborderRadius:(CGFloat)radius borderWidth:(CGFloat)border color:(UIColor*)color
{
    CALayer *layer = [self layer];
    layer.masksToBounds = YES;
    layer.cornerRadius = radius;
    layer.borderWidth = border;
    layer.borderColor = color.CGColor;
}

@end
