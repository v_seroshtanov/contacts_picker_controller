//
//  NSDictionary+RKTContacts.h
//  ContactFrameworkTest
//
//  Created by Виталий Сероштанов on 10.07.17.
//  Copyright © 2017 Виталий Сероштанов. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (RKTContacts)
- (id)rkt_objectForKeyNotNull:(id)key;
@end
