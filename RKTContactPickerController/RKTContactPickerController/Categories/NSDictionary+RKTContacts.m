//
//  NSDictionary+RKTContacts.m
//  ContactFrameworkTest
//
//  Created by Виталий Сероштанов on 10.07.17.
//  Copyright © 2017 Виталий Сероштанов. All rights reserved.
//

#import "NSDictionary+RKTContacts.h"

@implementation NSDictionary (RKTContacts)
- (id)rkt_objectForKeyNotNull:(id)key {
    id object = [self objectForKey:key];
    if (object == [NSNull null])
        return nil;
    
    return object;
}

@end
