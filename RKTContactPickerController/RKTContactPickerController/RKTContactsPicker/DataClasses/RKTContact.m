//
//  RKTContact.m
//  ContactFrameworkTest
//
//  Created by Виталий Сероштанов on 10.07.17.
//  Copyright © 2017 Виталий Сероштанов. All rights reserved.
//

#import "RKTContact.h"
#import <UIKit/UIKit.h>
@implementation RKTContact

-(NSString *) description
{
    NSString *result = [NSString stringWithFormat:@"contact name: %@ phone: %@ imageData: %@", self.name, self.phoneNumber, NSStringFromCGSize(self.photo.size)];
    return result;
}

@end
