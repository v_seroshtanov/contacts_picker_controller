//
//  RKTContactsPicker.h
//  ContactFrameworkTest
//
//  Created by Виталий Сероштанов on 07.07.17.
//  Copyright © 2017 Виталий Сероштанов. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RKTContactsPickerDelegate.h"

@interface RKTContactsPicker : UIViewController

@property (weak, nonatomic) id <RKTContactsPickerDelegate> delegate;

-(instancetype) init;

@property (nonatomic) BOOL singleSelection; // default NO

@property (strong, nonatomic) UIImage *customEmptyPhotoImage;
@property (strong, nonatomic) UIImage *customContactSelectedImage;
@property (strong, nonatomic) UIImage *customContactUnselectedImage;

@end
