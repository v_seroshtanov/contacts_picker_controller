//
//  RKTContactsTableViewCell.m
//  ContactFrameworkTest
//
//  Created by Виталий Сероштанов on 10.07.17.
//  Copyright © 2017 Виталий Сероштанов. All rights reserved.
//

#import "RKTContactsTableViewCell.h"
#import "UIView+RKTContacts.h"

@interface RKTContactsTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *tubeImage;

@end

@implementation RKTContactsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.tubeImage.image = [self.tubeImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tubeImage setTintColor:[UIColor lightGrayColor]];
    
    [self.photoImageView roundWithBborderRadius:self.photoImageView.frame.size.width/2
                                    borderWidth:0.0f
                                          color:nil];
}


@end
