//
//  RKTContactsCommonHelper.h
//  RKTContactPickerController
//
//  Created by Виталий Сероштанов on 10.07.17.
//  Copyright © 2017 Виталий Сероштанов. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIImage;

@interface RKTContactsCommonHelper : NSObject
+(NSBundle *) frameworkBundle;
+(UIImage *) defaultContactSelectImage;
+(UIImage *) defaultContactUnselectImage;
+(UIImage *) defaultEmptyContactImage;
@end
