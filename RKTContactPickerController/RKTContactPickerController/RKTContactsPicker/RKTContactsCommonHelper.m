//
//  RKTContactsCommonHelper.m
//  RKTContactPickerController
//
//  Created by Виталий Сероштанов on 10.07.17.
//  Copyright © 2017 Виталий Сероштанов. All rights reserved.
//

#import "RKTContactsCommonHelper.h"
#import <UIKit/UIKit.h>


@implementation RKTContactsCommonHelper
+(NSBundle *) frameworkBundle
{
    NSString* frameworkBundleID  = @"RKT.RKTContactPickerController";
    NSBundle* bundle = [NSBundle bundleWithIdentifier:frameworkBundleID];
    return bundle;
}


+(UIImage *) defaultContactSelectImage
{
    return [UIImage imageNamed:@"сhecked" inBundle:[RKTContactsCommonHelper frameworkBundle] compatibleWithTraitCollection:nil];
}


+(UIImage *) defaultContactUnselectImage
{
    return [UIImage imageNamed:@"unсhecked" inBundle:[RKTContactsCommonHelper frameworkBundle] compatibleWithTraitCollection:nil];
}


+(UIImage *) defaultEmptyContactImage
{
    return [UIImage imageNamed:@"anonim" inBundle:[RKTContactsCommonHelper frameworkBundle] compatibleWithTraitCollection:nil];
}

@end
