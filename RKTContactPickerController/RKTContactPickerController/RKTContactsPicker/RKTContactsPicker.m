//
//  RKTContactsPicker.m
//  ContactFrameworkTest
//
//  Created by Виталий Сероштанов on 07.07.17.
//  Copyright © 2017 Виталий Сероштанов. All rights reserved.
//

#import "RKTContactsPicker.h"
#import "RKTContact.h"
#import "RKTContactsTableViewCell.h"
#import "NSDictionary+RKTContacts.h"
#import "RKTContactsCommonHelper.h"

#define IMAGE_ANONIM [self emptyAvatar]
#define IMAGE_CHECKED  [self selectImage]
#define IMAGE_UNCHECKED [self unselectImage]

@import Contacts;

@interface RKTContactsPicker () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;

@property (strong, nonatomic) NSArray <RKTContact *> *contacts;
@property (strong, nonatomic) NSMutableDictionary *indexingAllContacts;

@property (strong, nonatomic) NSArray <NSString *> *keys;

@property (strong, nonatomic) CNContactStore *contactStore;
@property (strong, nonatomic) NSMutableArray *selectedContacts;

@end

@implementation RKTContactsPicker

-(instancetype) init
{
    self = [self initWithNibName:@"RKTContactsPicker" bundle:[RKTContactsCommonHelper frameworkBundle]];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.selectedContacts = [[NSMutableArray alloc] init];
    [self.tableView registerNib:[UINib nibWithNibName:@"RKTContactsTableViewCell" bundle:[RKTContactsCommonHelper frameworkBundle]] forCellReuseIdentifier:@"Cell"];
    [self setupContacts];

    if (self.navigationController != nil) {
        [self configureNavigationBar];
    }
}

#pragma mark - table view

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.keys count];
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *sectionName = _keys[section];
    NSArray *allValues = _indexingAllContacts[sectionName];
    return allValues.count;
}



-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RKTContactsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    NSString *sectionName = _keys[indexPath.section];
    NSArray <RKTContact *>*allValues = _indexingAllContacts[sectionName];
    RKTContact *contact = allValues[indexPath.row];
    cell.nameLabel.text = contact.name;
    cell.phoneLabel.text = contact.phoneNumber;

    cell.photoImageView.image = (contact.photo != nil) ?  contact.photo : IMAGE_ANONIM;
    cell.selectedImageView.image = [self.selectedContacts containsObject:contact] ? IMAGE_CHECKED : IMAGE_UNCHECKED;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *sectionName = _keys[indexPath.section];
    NSArray <RKTContact *>*allValues = _indexingAllContacts[sectionName];
    RKTContact *contact = allValues[indexPath.row];

    if (_singleSelection) {
        [_selectedContacts removeAllObjects];
        [_selectedContacts addObject:contact];
        [self.tableView reloadData];
    }else
    {
        [_selectedContacts containsObject:contact] ? [_selectedContacts removeObject:contact] : [_selectedContacts addObject:contact];
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
}


- (nullable NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return _keys;
}

-(void) configureNavigationBar
{
    self.title = @"Мои контакты";
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelButtonPressed:)];
    self.navigationItem.leftBarButtonItem = cancelButton;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonPressed:)];
    self.navigationItem.rightBarButtonItem = doneButton;
}

#pragma mark -  actions

- (IBAction)doneButtonPressed:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(contactsPicker:didFinishSelectContacts:)]) {
        [self.delegate contactsPicker:self didFinishSelectContacts:_selectedContacts];
    }
    
    if (self.navigationController != nil) {
        [self.navigationController popViewControllerAnimated:YES];
    } else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)cancelButtonPressed:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(contactsPickerDidCancel:)]) {
        [self.delegate contactsPickerDidCancel:self];
    }
    if (self.navigationController != nil) {
        [self.navigationController popViewControllerAnimated:YES];
    } else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - contacts

-(void) setupContacts
{
    self.contactStore  = [[CNContactStore alloc] init];
    
    NSArray *keyToFetch = @[CNContactEmailAddressesKey,CNContactFamilyNameKey,CNContactGivenNameKey,CNContactPhoneNumbersKey,CNContactPostalAddressesKey,CNContactThumbnailImageDataKey]; //contacts list key params to access using contacts.framework
    
    CNContactFetchRequest *fetchRequest = [[CNContactFetchRequest alloc] initWithKeysToFetch:keyToFetch]; //Contacts fetch request parrams object allocation
    
    NSMutableArray <RKTContact *>*contacts = [[NSMutableArray alloc] init];
    
    [_contactStore enumerateContactsWithFetchRequest:fetchRequest error:nil usingBlock:^(CNContact * _Nonnull contact, BOOL * _Nonnull stop) {
         NSArray *thisOne = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
        for (NSString *phoneNumber in thisOne) {
            RKTContact *newContact = [[RKTContact alloc] init];
            newContact.phoneNumber = phoneNumber;
            newContact.name = contact.familyName != nil ? [NSString stringWithFormat:@"%@ %@", contact.givenName, contact.familyName] : contact.givenName;
            newContact.photo = [UIImage imageWithData:contact.thumbnailImageData];
            [contacts addObject:newContact];
        }
    }];
    
    [contacts sortUsingComparator:^NSComparisonResult(RKTContact * _Nonnull obj1,RKTContact * _Nonnull obj2) {
        return [obj1.name compare:obj2.name];
    }];
    _contacts = contacts;
    
    self.indexingAllContacts = nil;
    self.indexingAllContacts = [[NSMutableDictionary alloc] init];
    
    NSString * currentSectionName = nil;
    
    for (RKTContact *contact in contacts) {
        NSString *section = [contact.name substringToIndex:1];
        NSMutableArray *currentSectionArray = nil;
        if ([section isEqualToString:currentSectionName]) {
            currentSectionArray = [[_indexingAllContacts rkt_objectForKeyNotNull:section] mutableCopy];
        } else
        {
            currentSectionName = section;
            currentSectionArray = [[NSMutableArray alloc] init];
        }
        [currentSectionArray addObject:contact];
        [_indexingAllContacts setObject:currentSectionArray forKey:section];
    }
    
    self.keys = [[_indexingAllContacts allKeys] sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2];
    }];

    [self.tableView reloadData];
    
}


#pragma mark - images


-(UIImage *) selectImage
{
    return (_customContactSelectedImage == nil) ? [RKTContactsCommonHelper defaultContactSelectImage] : _customContactSelectedImage;
    
}

-(UIImage *) unselectImage
{
    return (_customContactUnselectedImage == nil) ? [RKTContactsCommonHelper defaultContactUnselectImage] : _customContactUnselectedImage;
}

-(UIImage *) emptyAvatar
{
    return (_customEmptyPhotoImage == nil) ? [RKTContactsCommonHelper defaultEmptyContactImage] : _customEmptyPhotoImage;
}
@end
