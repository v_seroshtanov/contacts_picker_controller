//
//  RKTContactsPickerDelegate.h
//  ContactFrameworkTest
//
//  Created by Виталий Сероштанов on 10.07.17.
//  Copyright © 2017 Виталий Сероштанов. All rights reserved.
//

#import <Foundation/Foundation.h>
@class RKTContactsPicker;
@class RKTContact;


@protocol RKTContactsPickerDelegate <NSObject>

@optional

-(void) contactsPicker:(RKTContactsPicker *) picker didFinishSelectContacts: (NSArray <RKTContact *>*) contacts;
-(void) contactsPickerDidCancel:(RKTContactsPicker *) picker;

@end
