//
//  RKTContactPickerController.h
//  RKTContactPickerController
//
//  Created by Виталий Сероштанов on 10.07.17.
//  Copyright © 2017 Виталий Сероштанов. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RKTContactsPicker.h"
#import "RKTContact.h"

//! Project version number for RKTContactPickerController.
//FOUNDATION_EXPORT double RKTContactPickerControllerVersionNumber;

//! Project version string for RKTContactPickerController.
//FOUNDATION_EXPORT const unsigned char RKTContactPickerControllerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RKTContactPickerController/PublicHeader.h>


