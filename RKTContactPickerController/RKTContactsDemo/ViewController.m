//
//  ViewController.m
//  RKTContactsDemo
//
//  Created by Виталий Сероштанов on 10.07.17.
//  Copyright © 2017 Виталий Сероштанов. All rights reserved.
//

#import "ViewController.h"
#import "RKTContactPickerController.h"

@interface ViewController () <RKTContactsPickerDelegate>
@property (weak, nonatomic) IBOutlet UISwitch *singleSelectSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *customAvatar;
@property (weak, nonatomic) IBOutlet UISwitch *customSelectImage;
@property (weak, nonatomic) IBOutlet UISwitch *customUnselectImage;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"RKTContactPicker demo";
}

- (IBAction)goButtonPressed:(id)sender {
    RKTContactsPicker *picker = [[RKTContactsPicker alloc] init];
    picker.delegate = self;
    picker.singleSelection = self.singleSelectSwitch.on;
    picker.customContactSelectedImage = (self.customSelectImage.on) ? [UIImage imageNamed:@"favourite"]:nil;
    picker.customEmptyPhotoImage= (self.customAvatar.on) ? [UIImage imageNamed:@"face"]:nil;
    picker.customContactUnselectedImage = (self.customUnselectImage.on) ? [UIImage imageNamed:@"favorite_border"]:nil;
    [self presentViewController:picker animated:YES completion:nil];
}

-(void)contactsPickerDidCancel:(RKTContactsPicker *)picker
{
    NSLog(@"Cancelled");
}

-(void) contactsPicker:(RKTContactsPicker *)picker didFinishSelectContacts:(NSArray<RKTContact *> *)contacts
{
    NSLog(@"Got contacts: %@", contacts);
}


@end
